select *
FROM extRecordings

-- uh oh, i am basing the scrape on the MAX existing ident IN extRecordings
but i can envision a situation IN which a recording with ident 153 IS finished at
a point IN time AND the row EXISTS IN recorder.recordings, but a recording that
will have the ident of 152 IS NOT finished at that point IN time, so there IS 
no row, the next scrape will be based on ident > 153, there BY leaving out 152

so, DO a scrape INTO a tmp TABLE for the entire day, THEN DO a MERGE with extRecordings
based on ident

INSERT INTO extRecordings
SELECT *
FROM tmpExtRecordings a
WHERE NOT EXISTS (
  SELECT 1
  FROM extRecordings
  WHERE ident = a.ident);
 



INSERT INTO callCopyRecordings
SELECT ident, cast(timestampadd(sql_tsi_second, cast(a.recording_time AS sql_integer), '1970-01-01 00:00:00') AS sql_date) AS theDate,
  a.device_id AS extension, filename, substring(replace(a.user1, '-', ''), 3, 15) AS guid
-- SELECT COUNT(*)  
FROM extRecordings a
LEFT JOIN ( -- the anomalous dups
  SELECT thedate, extension, guid 
  FROM (
    SELECT cast(timestampadd(sql_tsi_second, cast(recording_time AS sql_integer), '1970-01-01 00:00:00') AS sql_date)AS theDate,
      agent_number AS extension, filename, 
      substring(replace(user1, '-', ''), 3, 15) AS guid
    FROM extRecordings) x 
  GROUP BY thedate, extension, guid HAVING COUNT(*) > 1) b 
    on cast(timestampadd(sql_tsi_second, cast(a.recording_time AS sql_integer), '1970-01-01 00:00:00') AS sql_date) = b.thedate
      AND a.device_id = b.extension
      AND substring(replace(a.user1, '-', ''), 3, 15) = b.guid
WHERE -- exlude old irrelevant data
  cast(timestampadd(sql_tsi_second, cast(recording_time AS sql_integer), '1970-01-01 00:00:00') AS sql_date) > '04/25/2014'      
AND b.thedate IS NULL   
AND NOT EXISTS ( -- new recordings only
  SELECT 1
  FROM callCopyRecordings
  WHERE ident = a.ident);

SELECT * FROM extRecordings ORDER BY ident desc  

