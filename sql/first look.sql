SELECT a.id, a.startTime, a.endTime, a.extension, b.*
FROM extCall a
LEFT JOIN extCallCopy b on LEFT(sipCallID, 7) = b.guidSegment1
  AND substring(sipCallID, 8, 4) = b.guidSegment2
  AND substring(sipCallID, 12, 4) = b.guidSegment3
WHERE CAST(startTime AS sql_date) = '05/14/2014'
  AND a.callType = 3

SELECT filename FROM (
SELECT a.id, a.startTime, a.endTime, a.extension, b.*
FROM extCall a
LEFT JOIN extCallCopy b on LEFT(sipCallID, 7) = b.guidSegment1
  AND substring(sipCallID, 8, 4) = b.guidSegment2
  AND substring(sipCallID, 12, 4) = b.guidSegment3
WHERE CAST(startTime AS sql_date) = '05/14/2014'
  AND a.callType = 3
  AND b.guid IS NOT NULL
) x GROUP BY filename HAVING COUNT(*) > 1  


-- callcopy stores time AS seconds since 1/1/1970
SELECT timestampadd(sql_tsi_second, -1400499599, now())
FROM system.iota

SELECT cast(timestampadd(sql_tsi_second, 1400499599, '1970-01-01 00:00:00') AS sql_date) 
FROM system.iota


SELECT * FROM extRecordings

SELECT cast(timestampadd(sql_tsi_second, cast(recording_time AS sql_integer), '1970-01-01 00:00:00') AS sql_date)AS theDate,
  agent_number AS extension, filename, 
  replace(substring(user1, 3, 17), '-', '')
FROM extRecordings

SELECT a.starttime, a.durationSeconds, a.extension, a.durationSeconds, b.*
FROM extCall a
LEFT JOIN (
  SELECT cast(timestampadd(sql_tsi_second, cast(recording_time AS sql_integer), '1970-01-01 00:00:00') AS sql_date)AS theDate,
    agent_number AS extension, filename, 
    replace(substring(user1, 3, 17), '-', '') AS guid
  FROM extRecordings) b 
    on CAST(a.startTime AS sql_date) = b.thedate
      AND a.extension = b.extension
     AND trim(LEFT(a.sipCallId, 15)) = trim(b.guid) 
WHERE CAST(a.startTime AS sql_Date) BETWEEN curdate() - 14 AND curdate() - 1 
  AND a.calltype = 3
ORDER BY CAST(startTime AS sql_date) DESC 

SELECT * FROM extRecordings ORDER BY ident

SELECT thedate, min(ident), max(ident), COUNT(*)
FROM (
SELECT ident, cast(timestampadd(sql_tsi_second, cast(recording_time AS sql_integer), '1970-01-01 00:00:00') AS sql_date)AS theDate,
  agent_number AS extension, filename, 
  replace(substring(user1, 3, 17), '-', '')
FROM extRecordings
) x GROUP BY theDate


-- recordings per extension
SELECT extension, 
  SUM(CASE WHEN guid IS NOT NULL THEN 1 ELSE 0 END) AS recordings,
  COUNT(*) AS calls
FROM (
SELECT a.starttime, a.durationSeconds, a.extension, a.durationSeconds, b.*
FROM extCall a
LEFT JOIN (
  SELECT cast(timestampadd(sql_tsi_second, cast(recording_time AS sql_integer), '1970-01-01 00:00:00') AS sql_date)AS theDate,
    agent_number AS extension, filename, 
    replace(substring(user1, 3, 17), '-', '') AS guid
  FROM extRecordings) b 
    on CAST(a.startTime AS sql_date) = b.thedate
      AND a.extension = b.extension
     AND trim(LEFT(a.sipCallId, 15)) = trim(b.guid) 
WHERE CAST(a.startTime AS sql_Date) BETWEEN curdate() - 14 AND curdate() - 1 
  AND a.calltype = 3)x
GROUP BY extension  

-- core recording info

SELECT cast(timestampadd(sql_tsi_second, cast(recording_time AS sql_integer), '1970-01-01 00:00:00') AS sql_date)AS theDate,
  agent_number AS extension, filename, 
  substring(replace(user1, '-', ''), 3, 15) AS guid
FROM extRecordings
ORDER by recording_time

-- uh oh 
SELECT thedate, extension, guid FROM (
SELECT cast(timestampadd(sql_tsi_second, cast(recording_time AS sql_integer), '1970-01-01 00:00:00') AS sql_date)AS theDate,
  agent_number AS extension, filename, 
  substring(replace(user1, '-', ''), 3, 15) AS guid
FROM extRecordings
) x GROUP BY thedate, extension, guid HAVING COUNT(*) > 1
  
  
SELECT * FROM extRecordings WHERE substring(replace(user1, '-', ''), 3, 15) = '0080000B024529E' 

SELECT * FROM extCall WHERE sipcallid LIKE '0080000B024529E%'
SELECT * FROM extConnect WHERE calltableid = 1711781

1. discard the 2013 data, it IS garbage anyway, new call copy really started cooking
until 4/26/2014

SELECT cast(timestampadd(sql_tsi_second, cast(recording_time AS sql_integer), '1970-01-01 00:00:00') AS sql_date), COUNT(*)
FROM extRecordings a
GROUP BY cast(timestampadd(sql_tsi_second, cast(recording_time AS sql_integer), '1970-01-01 00:00:00') AS sql_date)

DELETE FROM extRecordings 
WHERE cast(timestampadd(sql_tsi_second, cast(recording_time AS sql_integer), '1970-01-01 00:00:00') AS sql_date) < '04/25/2014'

2.
maybe SELECT the one WHERE device_id = dnis?
-- nope, that does NOT look LIKE the answer
SELECT *
FROM (
-- uh oh 
  SELECT thedate, extension, guid FROM (
  SELECT cast(timestampadd(sql_tsi_second, cast(recording_time AS sql_integer), '1970-01-01 00:00:00') AS sql_date)AS theDate,
    agent_number AS extension, filename, 
    substring(replace(user1, '-', ''), 3, 15) AS guid
  FROM extRecordings
  ) x GROUP BY thedate, extension, guid HAVING COUNT(*) > 1) a
LEFT JOIN extRecordings b on a.thedate = cast(timestampadd(sql_tsi_second, cast(recording_time AS sql_integer), '1970-01-01 00:00:00') AS sql_date)
  AND a.extension = b.device_id
  AND a.guid =  substring(replace(user1, '-', ''), 3, 15)

3.
given the relative infrequency of the occurence, my tendency IS to DELETE AS anomalies
leave them IN the extRecordings, DO NOT put them IN the next level of table
  
  
  
-- calls AND recordings
SELECT a.starttime, a.durationSeconds, a.extension, left(sipcallid,15), b.*
FROM extCall a
LEFT JOIN (  
  SELECT ident, cast(timestampadd(sql_tsi_second, cast(recording_time AS sql_integer), '1970-01-01 00:00:00') AS sql_date)AS theDate,
    agent_number AS extension, filename, 
    substring(replace(user1, '-', ''), 3, 15) AS guid
  FROM extRecordings) b 
    on CAST(a.startTime AS sql_date) = b.thedate
      AND a.extension = b.extension
     AND LEFT(trim(a.sipCallId), 15) = b.guid  
WHERE CAST(a.startTime AS sql_Date) BETWEEN curdate() - 14 AND curdate()  
  AND a.calltype = 3     
ORDER BY starttime desc     